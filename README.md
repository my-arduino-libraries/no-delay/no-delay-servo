# No Delay - Servo

## Example

```cpp
#include <Arduino.h>
#include "NDServo.h"

const uint8_t my_servo_pin = 5;
const int interval = 25; // interval when to update the postion
NDServo my_servo(interval);

void setup(){
    Serial.begin(9600);

    my_servo.Attach(my_servo_pin);
}

void loop(){
    my_servo_pin.Update(90); // Set servo to mid-point
    delay(1000);
    my_servo_pin.UpdateMS(1500); // Set servo to mid-point
    delay(1000);
}
```