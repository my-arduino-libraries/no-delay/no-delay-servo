/*
    NDServo.cpp - Library for using Servo without delays.
    Created by bretagne.bastian@gmail.com in 2021
*/

#include <Arduino.h>
#include "NDServo.h"

NDServo::NDServo(int interval){
    updateInterval = interval;
}

void NDServo::Attach(int pin){
    servo.attach(pin);
}

void NDServo::Detach(){
    servo.detach();
}

void NDServo::Update(int pos){
    if((millis() - lastUpdate) > updateInterval){
        lastUpdate = millis();
        servo.write(pos);
    }
}

void NDServo::UpdateMS(int microseconds){
    if((millis() - lastUpdate) > updateInterval){
        lastUpdate = millis();
        servo.writeMicroseconds(microseconds);
    }
}
