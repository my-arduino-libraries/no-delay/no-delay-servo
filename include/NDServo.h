/*
    NDServo.h - Library for using Servo without delays.
    Created by bretagne.bastian@gmail.com in 2021
*/

#ifndef NDSERVO_H
#define NDSERVO_H

#include <Arduino.h>
#include "Servo.h"

class NDServo{
    private:
        Servo servo;
        unsigned long updateInterval;
        unsigned long lastUpdate;

    public:
        NDServo(int interval);
        void Attach(int pin);
        void Detach();
        void Update(int pos);
        void UpdateMS(int microseconds);

};

#endif